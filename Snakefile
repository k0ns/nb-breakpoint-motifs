#conda env export > motifs_conda_env.yml
#snakemake --forceall --use-conda

import os
import sys
import pandas as pd

CALLSETINFO = pd.read_csv("CALLSETINFO.csv", sep=";", header=0)
CALLSETS = CALLSETINFO.Callset.tolist()
MOTIFINFO = pd.read_csv("MotifsInfo.csv", sep=";", header=0)
MOTIFS = MOTIFINFO.MotifName.tolist()
NRANDOM = 10

rule all:
    input:
        expand("FimoOutput/{s}_Real.{m}/fimo.tsv", s=CALLSETS, m=MOTIFS),
        expand("FimoOutput/{s}_Random{i}.{m}/fimo.tsv", s=CALLSETS, m=MOTIFS, i=[str(i) for i in range(1,NRANDOM+1)]),
        expand("Motifs/{m}.eps", m=MOTIFS)

rule logos:
    input:
        "{motif}.txt"
    output:
        "{motif}.eps"
    conda:
        "motifs_conda_env.yml"
    shell:
        "ceqlogo -i {input} -m 1 -o {output} -f EPS" # -m assumes that there is only one motif in the motif file

rule shuffle_motifs:
    input:
        "Motifs/RPE_PSS_fromSupp.txt"
    output:
        "Motifs/RPE_PSS_fromSupp_Shuffled{N}.txt"
    conda:
        "motifs_conda_env.yml"
    shell:
        "motif-shuffle-columns -o {output} {input}"

rule get_random_sequences:
    input:
        "BreakpointSequences/{Callset}_Real_NB_bp.fa"
    output:
        "BreakpointSequences/{Callset}_Random{N}_NB_bp.fa"
    params:
        Callset = "{Callset}",
        N = "{N}"
    conda:
        "motifs_conda_env.yml"
    shell:
        "Rscript CreateRandomSequences.R {params.Callset}_Real {input} {params.Callset}_Random{params.N} {output}"

rule get_background:
    input:
        "BreakpointSequences/{Callset}_NB_bp.fa"
    output:
        "MarkovBackgroundModels/{Callset}_MarkovBackgroundModel.txt"
    conda:
        "motifs_conda_env.yml"
    shell:
        "fasta-get-markov {input} {output}"

rule fimo:
    input:
        sequences_file = "BreakpointSequences/{Callset}_NB_bp.fa",
        motif_file = "Motifs/{Motif}.txt",
        background_model_file = "MarkovBackgroundModels/{Callset}_MarkovBackgroundModel.txt"
    output:
        "FimoOutput/{Callset}.{Motif}/fimo.tsv"
    params:
        callsetname = lambda wildcards: {wildcards.Callset},
        motifname = lambda wildcards: {wildcards.Motif}
    conda:
        "motifs_conda_env.yml"
    shell:
        "fimo --qv-thresh --thresh 15.0E-2 --oc FimoOutput/{params.callsetname}.{params.motifname} --bfile {input.background_model_file} {input.motif_file} {input.sequences_file}"

# rule analysis:
#     input:
#         "FimoOutput/" + CALLSETS[0] + "." +  MOTIFS[0] + "/fimo.tsv"
#     output:
#         "Figures/PSSPercent.pdf"
#     shell:
#         "Rscript AnalyseFIMO.R " + str(NRANDOM)
