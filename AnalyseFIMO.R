rm(list=ls())
library(dplyr)
library(tidyr)
library(ggplot2)
library(ggrepel)
library(Biostrings)
source("CustomThemes.R")

args = commandArgs(trailingOnly=TRUE)
nrandom = args[1]
nrandom = 10

WORKING_DIR = "/Volumes/Transcend/nb-breakpoint-motifs/"
callsetinfo = read.table(paste0(WORKING_DIR, "CallsetInfo.csv"),header=T,sep=";")
CALLSETS = paste0(as.character(callsetinfo$Callset))
motifsinfo = read.table(paste0(WORKING_DIR, "MotifsInfo.csv"),header=T,sep=";")
MOTIFS = as.character(motifsinfo$MotifName)
RANDOMORREAL_LEVELS = c("Real", paste0("Random", 1:nrandom))

sample_info = list()
i=1
for (this_callset in CALLSETS){
  for (this_randomorreal in RANDOMORREAL_LEVELS){
  this_callset_randomorreal = readDNAStringSet(paste0(WORKING_DIR, "BreakpointSequences/", this_callset, "_", this_randomorreal,"_NB_bp.fa")) %>%
    names() %>%
    as.data.frame() %>% as_tibble()
  colnames(this_callset_randomorreal) = "seqname"
  this_callset_randomorreal = this_callset_randomorreal %>% 
    mutate(seqname = as.character(seqname)) %>%
    tidyr::separate(seqname, 
                    into=c("ID", "Sample", "SVCaller", "RandomOrReal", "Info", "Chr", "Pos"), 
                    sep="_")
  sample_info[[i]] = this_callset_randomorreal %>% 
    group_by(Sample, SVCaller, RandomOrReal, Info) %>%
    summarise(AllRearrangements = n()) %>%
    ungroup() %>%
    mutate(Callset = this_callset) %>% 
    tidyr::complete(Sample, SVCaller, Info, fill=list("AllRearrangements"=0))
  i=i+1
  }
}
sample_info = do.call(rbind, sample_info)

random_info = sample_info %>% filter(grepl("Random", RandomOrReal))
sample_info = sample_info %>% filter(grepl("Real", RandomOrReal))

SAMPLES = unique(sample_info$Sample)

sample_info %>% group_by(Callset, RandomOrReal) %>% summarise(n=n())

df = list()
i=1
for (this_callset in CALLSETS){
  for (this_motif in MOTIFS){
    df[[i]] =
      tryCatch(
        read.table(paste0(WORKING_DIR, "FimoOutput/", this_callset, "_Real.", this_motif, "/fimo.tsv"),
                   header=T, sep="\t", comment.char = "#") %>%
          as_tibble() %>%
          dplyr::select(-motif_alt_id) %>%
          mutate(Callset = this_callset, motif = this_motif),
        error = function(e) NULL)
    i = i+1
    
    for (this_nrandom in 1:nrandom){
      df[[i]] =
        tryCatch(
          read.table(paste0(WORKING_DIR, "FimoOutput/", this_callset, "_Random", as.character(this_nrandom),".", this_motif, "/fimo.tsv"),
                     header=T, sep="\t", comment.char = "#") %>%
            as_tibble() %>%
            dplyr::select(-motif_alt_id) %>%
            mutate(Callset = this_callset, motif = this_motif),
          error = function(e) NULL)
      i = i+1
    }
    
  }
}
df = do.call(rbind, df)

df = df %>% 
  separate(sequence_name, 
           into=c("ID", "Sample", "SVCaller", "RandomOrReal", "Info", "Chr", "Pos"), sep="_") %>%
  mutate(isRandom = grepl("Random", RandomOrReal)) %>%
  mutate(Callset = factor(Callset, levels = CALLSETS),
         motif = factor(motif, levels = MOTIFS),
         Sample = factor(Sample, levels = SAMPLES),
         RandomOrReal = factor(RandomOrReal, levels=RANDOMORREAL_LEVELS))

hit_counts = 
  df %>%
  group_by(Callset, motif, RandomOrReal, isRandom) %>%
  summarise(n=n()) %>%
  ungroup() %>%
  tidyr::complete(Callset, motif, RandomOrReal, fill=list("n" = 0)) %>%
  mutate(isRandom = grepl("Random", as.character(RandomOrReal))) %>% 
  group_by(Callset, motif, isRandom) %>%
  summarise(mean_hits = mean(n), sd_hits = sd(n)) %>%
  mutate(SVCaller = Callset) 

random_hits = 
  df %>%
  filter(grepl("Random", RandomOrReal)) %>% # only take Random
  mutate(RandomOrReal = factor(RandomOrReal, levels=paste0("Random", 1:nrandom))) %>% 
  group_by(Callset, RandomOrReal, motif) %>%
  summarise(Hits=n()) %>%
  ungroup() %>%
  tidyr::complete(Callset, RandomOrReal, motif, fill=list("Hits" = 0)) %>%
  full_join(random_info) %>%
  mutate(RandomPercentHits = Hits/AllRearrangements)

random_hits_mean = 
  random_hits %>%
  group_by(Callset, motif) %>%
  summarise(MeanRandomPercentHits = mean(RandomPercentHits))

hits_per_sample = 
  df %>%
  filter(RandomOrReal == "Real") %>%
  group_by(Callset, SVCaller, Sample, motif) %>%
  summarise(Hits=n()) %>%
  ungroup() %>%
  tidyr::complete(Callset, SVCaller, Sample, motif, fill=list("Hits" = 0)) %>%
  full_join(sample_info) %>%
  mutate(PercentHits = Hits/AllRearrangements) %>%
  full_join(random_hits_mean) %>%
  mutate(PercentHitsFC = (PercentHits+0.0001)/(MeanRandomPercentHits+0.0001))


df %>% group_by(RandomOrReal, SVCaller, motif) %>% summarise(n=n()) %>% ungroup() %>%
  complete(RandomOrReal, SVCaller, motif, fill=list("n"=0)) %>% ggplot(aes(x=RandomOrReal, y=n)) + geom_col() + facet_grid(motif~SVCaller) +
  theme_kons1() + ggsave(paste0(WORKING_DIR, "Figures/NumberOfFIMOHits.pdf"), width=5, height=10)

